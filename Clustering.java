import java.util.*;

public class Clustering{
	public static void main(String[] args){
		double[] sourceData = {5,7,1,9,8,5,6,4,3,2,9,2,5,8};

		Kmeans kmeans = new Kmeans();
		kmeans.init(sourceData, 2);
		
		long start = System.currentTimeMillis();
		while(kmeans.stepExe());
		long end = System.currentTimeMillis();

		kmeans.printResult();
		System.out.println("time: " + (end-start)+"ms");

	}

}


final class Kmeans{
	private double[] originalData;
	private final List<Cluster> clusterList = new ArrayList<>();

	public Kmeans(){}

	public void init(final double[] originalData, final int clusterNum){
		this.clusterList.clear();
		this.originalData = originalData;

		final int dataCount = originalData.length;
		final int[] clusterNumbers = new int[dataCount];

		// 指定された数のクラスタを生成
		for(int i = 0; i < clusterNum; i++)
			this.clusterList.add(new Cluster(i));

		// 元データをランダムなクラスタに振り分ける
		for(int i = 0; i < dataCount; i++){
			clusterNumbers[i] = (int)(Math.random() * clusterNum);				 // clusterNumbers[i]はoriginalData[i]のクラスタ番号を示す
			this.clusterList.get(clusterNumbers[i]).addMember(originalData[i]); // それぞれのclusterオブジェクトに管理させる要素を渡す
		}
	}

	// クラスタリングを一回ずつ実行する [戻り値: 終了->false 未終了->true]
	public boolean stepExe(){
		for(int i = 0; i < this.clusterList.size(); i++){
			final Cluster targetCluster = clusterList.get(i);									

			int nearClusterNumber = i;
			for(int j = 0; j < targetCluster.getMembersSize(); j++){				// 要素数だけ繰り返し
				final double targetValue = targetCluster.getMember(j);				// 注目する要素をtargetValueとする
				double min = Math.abs(targetCluster.getAverage() - targetValue); 	//リスト内要素の平均値から要素を差し引いたものを最小と設定する
				if(min == 0) continue; 												// 最小距離が0であれば更新があり得ない

				for(int k = 0; k < this.clusterList.size(); k++){
					final double tmp = Math.abs(this.clusterList.get(k).getAverage() - targetValue );
					if(min > tmp){
						min = tmp;
						nearClusterNumber = k;
					}

				}
				if(nearClusterNumber != i){
					this.clusterList.get(nearClusterNumber).addMember(targetCluster.removeMember(j));
					return true;
				}

			}

		}
		return false;
	}

	public void printResult(){
		for(Cluster c : clusterList)
			c.printClusterList();
	}



	private final class Cluster{
		private final int clusterNumber;
		private final List<Double> values = new ArrayList<>();

		//private int hash_values;	// リストvaluesのハッシュ値を保持し、キャッシュが適切かの判定に利用する
		//private double cache_Avg;	// getAverageメソッドの結果をキャッシュする変数

		Cluster(int clusterNumber){
			this.clusterNumber = clusterNumber;
		}
		public void addMember(double d){
			this.values.add(d);
			//this.hash_values = this.values.hashCode();
		}
		public double getMember(int index){
			return this.values.get(index);
		}
		public int getMembersSize(){
			return this.values.size();
		}
		public double removeMember(int index){	
			//Double d = this.values.remove(index);
			//this.hash_values = this.values.hashCode();

			//return d;
			return this.values.remove(index);
		}
		public void printClusterList(){
			System.out.println("Cluster "+this.clusterNumber+" :"+this.values+"\n");
		}

		// TODO: ループ内におけるオートボクシング
		public double getAverage(){
			/*
			final int tmpHash = this.values.hashCode();
			if(this.hash_values == tmpHash) {
				return cache_Avg;
			}
			this.hash_values = tmpHash;
			//*/

			double sum = 0;
			for(double d : this.values) sum += d;	// オートボクシングがループ内で起こっているため低速

			//return this.cache_Avg = sum/this.values.size();
			return sum/this.values.size();
		}
	}
}
